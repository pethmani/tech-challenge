﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.DataAccess
{   
    public interface IMediaDataAccess
    {
        List<Media.Entity.Media> GetMediaList();
        List<Media.Entity.Cast> GetCastList();
        List<Media.Entity.Genre> GetGenreList();
        List<Media.Entity.Award> GetAwardList();
        List<Media.Entity.StoryLine> GetStoryLineList();
    }
}
