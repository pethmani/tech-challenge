﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Media.Entity;
namespace Media.DataAccess
{
    public class SerialDataAccess : IMediaDataAccess
    {   //Testing Sample Data with MediaList.
        public List<Entity.Media> GetMediaList()
        {
            List<Entity.Media> mediaList = new List<Entity.Media>();
            mediaList.Add(new Entity.Media() { TitleId = 1, ReleaseYear = 2009, TitleName = "True Detective" });
            mediaList.Add(new Entity.Media() { TitleId = 2, ReleaseYear = 2013, TitleName = "Orange is the New Black" });
            return mediaList;
        }


        public List<Entity.Cast> GetCastList()
        {
            throw new NotImplementedException();
        }

        public List<Entity.Genre> GetGenreList()
        {
            throw new NotImplementedException();
        }

        public List<Entity.Award> GetAwardList()
        {
            throw new NotImplementedException();
        }

        public List<Entity.StoryLine> GetStoryLineList()
        {
            throw new NotImplementedException();
        }
    }
}
