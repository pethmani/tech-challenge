﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Media.Data;
using Media.Data.Repository;
using Media.Entity;
namespace Media.DataAccess
{
    public class MovieDataAccess : IMediaDataAccess
    {
        MediaData mediaData = new MediaData();        
        // Get the Media List Title like Movies and Serials.
        public List<Media.Entity.Media> GetMediaList()
        {
            List<Title> mediaTitles = mediaData.GetData().Titles.ToList();
            List<Media.Entity.Media> mediaList = new List<Media.Entity.Media>();

            foreach (var title in mediaTitles)
            {
                Media.Entity.Media media = new Media.Entity.Media();
                media.TitleName = title.TitleName;
                media.TitleId = title.TitleId;
                media.ReleaseYear = title.ReleaseYear;
                mediaList.Add(media);
            }

            return mediaList;
        }

        // Get the List of Cast in the Movie or Serials
        public List<Media.Entity.Cast> GetCastList()
        {
            List<TitleParticipant> mediaParticipants = mediaData.GetData().TitleParticipants.ToList();
            List<Media.Entity.Cast> castList = new List<Media.Entity.Cast>();

            foreach (var participant in mediaParticipants)
            {
                Media.Entity.Cast cast = new Media.Entity.Cast();
                cast.TitleId = participant.TitleId;
                cast.TitleName = participant.Title.TitleName;
                cast.Name = participant.Participant.Name;
                cast.Role = participant.RoleType;
                castList.Add(cast);                    
            }

             return castList;
        }

        //Get the List of Genre in Move or Serials. 
        public List<Media.Entity.Genre> GetGenreList()
        {
            List<TitleGenre> mediaGenre = mediaData.GetData().TitleGenres.ToList();
            List<Media.Entity.Genre> genreList = new List<Media.Entity.Genre>();

            foreach (var item in mediaGenre)
            {
                Media.Entity.Genre genre = new Media.Entity.Genre();
                genre.TitleId = item.Title.TitleId;
                genre.Name = item.Genre.Name;
                genreList.Add(genre);
            
            }

            return genreList;
        }

        // Get the List of StoryLine for Movies or Serials.
        public List<Media.Entity.StoryLine> GetStoryLineList()
        {
            List<Media.Data.Repository.StoryLine> mediaStoryLine = mediaData.GetData().StoryLines.ToList();
            List<Media.Entity.StoryLine> storyLineList = new List<Media.Entity.StoryLine>();

            foreach (var item in mediaStoryLine)
            {
                Media.Entity.StoryLine storyLine = new Media.Entity.StoryLine();
                storyLine.TitleId = item.TitleId;
                storyLine.TitleName = item.Title.TitleName;
                storyLine.Language = item.Language;
                storyLine.Type = item.Type;
                storyLine.Description = item.Description;
                storyLineList.Add(storyLine);
            }

            return storyLineList;
        }

        // Get the List of Awards in Movies or Serials
        public List<Media.Entity.Award> GetAwardList()
        {
            List<Media.Data.Repository.Award> mediaAward = mediaData.GetData().Awards.ToList();
            List<Media.Entity.Award> awardList = new List<Media.Entity.Award>();

            foreach (var item in mediaAward)
            {
                Media.Entity.Award award = new Media.Entity.Award();
                award.TitleId = item.TitleId;
                award.TitleName = item.Title.TitleName;
                award.AwardName = item.Award1;
                award.AwardCompany = item.AwardCompany;
                award.AwardYear = item.AwardYear;              

                awardList.Add(award);
            }

            return awardList;
        }


    }
}
