﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Media.Web.Models;
using Movie.Web.MediaServiceReference;


namespace Media.Web.Controllers
{
    public class TitlesController : Controller
    {
        // Invoking Media Service Instances.
        public MediaServiceClient mediaService =  new MediaServiceClient();        
        // Loading all the Titles.
        public ActionResult Index(int? type)
        {
            if (type == null) type = 1;                        
            return View(mediaService.GetMediaList(type.Value));         
        }
        //Filtering Title List by Title
        [HttpPost]
        public ActionResult Index(string TitleSearch,int? type)
        {
            if (type == null) type = 1;            
            var Title = mediaService.GetMediaList(type.Value).Where(t => t.TitleName.ToLower().Contains(TitleSearch.ToLower())).ToList();

            return View(Title);      
        }
        // Getting Title Details for single Title
        public ActionResult Details(int id = 0,int type =1)
        {
            string GenreName = "";
           
            var Title = mediaService.GetMediaList(type).Where(t => t.TitleId == id).First();

            if (Title == null)
            {
                return HttpNotFound();
            }
            else
            {
                foreach (var n in mediaService.GetGenreList(type).Where(t => t.TitleId == id).ToList())
                {
                    GenreName = GenreName + n.Name + " | ";
                }

                ViewBag.GenreName = GenreName.Remove(GenreName.LastIndexOf("|"));

            }
            return View(Title);
        }
        //// Getting Award Details for Title
        public ActionResult AwardDetails(int TitleId = 0,int type =1)
        {
            var Award = mediaService.GetAwardList(type).Where(t => t.TitleId == TitleId).ToList();
            if (Award == null)
            {
                return HttpNotFound();
            }
            else
            {
              ViewBag.TitleName = mediaService.GetMediaList(type).Where(t => t.TitleId == TitleId).Select(c => c.TitleName).Single();
            }
                 
            return View(Award);
        }
        //// Getting Cast Details for Title
        public ActionResult Cast(int TitleId = 0, int type = 1)
        {
            var Cast = mediaService.GetCastList(type).Where(t => t.TitleId == TitleId).ToList();
            if (Cast == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.TitleName = mediaService.GetMediaList(type).Where(t => t.TitleId == TitleId).Select(c => c.TitleName).Single();
            }
            return View(Cast);
        }
        // //Getting StoryLines Details for Title
        public ActionResult StoryLines(int TitleId = 0, int type = 1)
        {
                     
            var StoryLines = mediaService.GetStoryLineList(type).Where(t => t.TitleId == TitleId).ToList();
            if (StoryLines == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.TitleName = mediaService.GetMediaList(type).Where(t => t.TitleId == TitleId).Select(c => c.TitleName).Single();
            }
            return View(StoryLines);
        }
        
    }
}