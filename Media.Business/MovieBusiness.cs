﻿using Media.DataAccess;
using Media.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Media.Business
{
    public class MediaBusiness
    {   //Implemented the Factory Design Pattern .
        //Based on the Media Type will load the data in the Object.
        IMediaDataAccess mediaDA;

        public MediaBusiness(int type)
        {
            mediaDA = MediaFactory.GetMediaFactory(type);
        }

        public MediaBusiness(IMediaDataAccess da, bool isTest)
        {
            mediaDA = da;
        }

        public List<Media.Entity.Media> GetMediaList()
        {
            return mediaDA.GetMediaList();
        }

        public List<Cast> GetCastList()
        {
            return mediaDA.GetCastList();
        }

        public List<Genre> GetGenreList()
        {
            return mediaDA.GetGenreList();
        }

        public List<Award> GetAwardList()
        {
            return mediaDA.GetAwardList();
        }

        public List<StoryLine> GetStoryLineList()
        {
            return mediaDA.GetStoryLineList();
        }
    }
}