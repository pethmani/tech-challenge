﻿using Media.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.Business
{
    public class MediaFactory
    {   // Implemented the Factory Design Pattern.
        //Based on the Media Type will load the data in the Object.
        public static IMediaDataAccess GetMediaFactory(int type)
        {
            IMediaDataAccess mediaDA = null;

            if (type == 1)
            {
                mediaDA = new MovieDataAccess();
            }
            else if (type == 2)
            {
                mediaDA = new SerialDataAccess();
            }

            return mediaDA;
        }

    }
}
