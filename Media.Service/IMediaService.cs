﻿using Media.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Media.Service
{
    [ServiceContract]
    public interface IMediaService
    {
        [OperationContract]
        List<Media.Entity.Media> GetMediaList(int Type);
        [OperationContract]
        List<Cast> GetCastList(int Type);
        [OperationContract]
        List<Genre> GetGenreList(int Type);
        [OperationContract]
        List<Award> GetAwardList(int Type);
        [OperationContract]
        List<StoryLine> GetStoryLineList(int Type);       
    }
       
}
