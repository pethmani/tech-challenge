﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Media.Business;
using Media.Entity;

namespace Media.Service
{
     public class MediaService : IMediaService
    {
        public MediaBusiness movieBL = null;        

        public List<Entity.Media> GetMediaList(int type)
        {
            movieBL = new MediaBusiness(type);

            return movieBL.GetMediaList();
        }

        public List<Entity.Cast> GetCastList(int type)
        {
            movieBL = new MediaBusiness(type);

            return movieBL.GetCastList();
        }

        public List<Entity.Genre> GetGenreList(int type)
        {
            movieBL = new MediaBusiness(type);

            return movieBL.GetGenreList();
        }

        public List<Entity.Award> GetAwardList(int type)
        {
            movieBL = new MediaBusiness(type);

            return movieBL.GetAwardList();
        }

        public List<Entity.StoryLine> GetStoryLineList(int type)
        {
            movieBL = new MediaBusiness(type);

            return movieBL.GetStoryLineList();
        }
    }
}
