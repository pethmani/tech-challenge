﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Media.Business;
using Movie.Test.TestClasses;
using System.Linq;

namespace Movie.Test
{
    [TestClass]
    public class MovieBusinessTest
    {
         //Testing MediaList TileName.
        [TestMethod]
        public void GetMediaListBusinessTest()
        {
            MediaBusiness movieBusiness = new MediaBusiness(new MediaTestDataAccess(), true);
            var movieList = movieBusiness.GetMediaList();
            Assert.AreEqual(movieList.First().TitleName, "Titanic");

        }
    }
}
