﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.Entity
{
    public class Genre
    {
        public int TitleId { get; set; }
        public string Name { get; set; }
    }
}
