﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.Entity
{
    public class Media
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }   
        public Nullable<int> ReleaseYear { get; set; }
    }
}
