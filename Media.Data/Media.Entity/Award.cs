﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.Entity
{
    public class Award
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public Nullable<int> AwardYear { get; set; }
        public string AwardName { get; set; }
        public string AwardCompany { get; set; }       
       
    }
}
